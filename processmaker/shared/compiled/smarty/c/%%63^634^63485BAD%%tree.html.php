<?php /* Smarty version 2.6.18, created on 2014-11-26 10:51:47
         compiled from tree.html */ ?>
<?php if (( $this->_tpl_vars['node']->nodeType === 'base' )): ?>
<div class="treeBase" style="width:<?php echo $this->_tpl_vars['node']->contentWidth; ?>
">
	<div class="boxTop">
		<div class="a"></div>
		<div class="b"></div>
		<div class="c"></div>
	</div>
  <div class="content" style= "overflow-y: auto; overflow-x: hidden; height: <?php echo $this->_tpl_vars['node']->height; ?>
; ">
  <table id="treeNode[<?php echo $this->_tpl_vars['node']->name; ?>
]" cellspacing="0" cellpadding="0" border="0" style="width:<?php echo $this->_tpl_vars['node']->width; ?>
;">
  <tr <?php if ($this->_tpl_vars['node']->value == ''): ?>style="display:none"<?php endif; ?>>
  	<td class="a" name="treeNode[plus][<?php echo $this->_tpl_vars['node']->name; ?>
]" style="background-color:white;">
  		<?php echo $this->_tpl_vars['node']->printPlus(); ?>

  	</td>
  	<td class="b" name="treeNode[label][<?php echo $this->_tpl_vars['node']->name; ?>
]">
  	  <?php echo $this->_tpl_vars['node']->printLabel(); ?>

  	</td>
  </tr>
  <tr <?php if ($this->_tpl_vars['node']->contracted): ?>style="display:none"<?php endif; ?>>
  	<td class="c"></td>
	  <td class="d" name="treeNode[content][<?php echo $this->_tpl_vars['node']->name; ?>
]">
  	  <?php echo $this->_tpl_vars['node']->printContent(); ?>

  	</td>
  </tr>
  </table>
  </div>
	
	<div class="boxBottom">	
		<div class="a"></div>
		<div class="b"></div>
		<div class="c"></div>	
	</div>
</div>
<?php elseif (( $this->_tpl_vars['node']->nodeType === 'parent' )): ?>
<div class="treeParent">
	<table id="treeNode[<?php echo $this->_tpl_vars['node']->name; ?>
]" cellspacing="0" cellpadding="0" border="0" style="width:<?php echo $this->_tpl_vars['node']->width; ?>
;border-collapse:collapse;">
	 <tr <?php if ($this->_tpl_vars['node']->value == ''): ?>style="display:none"<?php endif; ?>>
	 	<td class="xa" name="treeNode[plus][<?php echo $this->_tpl_vars['node']->name; ?>
]" width="15px">
	 		<?php echo $this->_tpl_vars['node']->printPlus(); ?>

	 	</td>
	 	<td class="xb" name="treeNode[label][<?php echo $this->_tpl_vars['node']->name; ?>
]">
	  	  <?php echo $this->_tpl_vars['node']->printLabel(); ?>

	  	</td>
	  </tr>
	  <tr <?php if ($this->_tpl_vars['node']->contracted): ?>style="display:none"<?php endif; ?>>
	  	<td class="d"></td>
		  <td class="xd" name="treeNode[content][<?php echo $this->_tpl_vars['node']->name; ?>
]">
  		  <?php echo $this->_tpl_vars['node']->printContent(); ?>

  		</td>
	  </tr>
	 </table>
 </div>
<?php elseif (( $this->_tpl_vars['node']->nodeType === 'parentBlue' )): ?>
<div class="treeParentBlue">
<div class="boxTopBlue">
	<div class="a"></div>
	<div class="b"></div>
	<div class="c"></div>
</div>
<div class="boxContentBlue">
	<table id="treeNode[<?php echo $this->_tpl_vars['node']->name; ?>
]" cellspacing="0" cellpadding="0" border="0" style="width:<?php echo $this->_tpl_vars['node']->width; ?>
;border-collapse:collapse;">
	 <tr <?php if ($this->_tpl_vars['node']->value == ''): ?>style="display:none"<?php endif; ?>>
	 	<td class="xa" name="treeNode[plus][<?php echo $this->_tpl_vars['node']->name; ?>
]" width="15px">
	 		<?php echo $this->_tpl_vars['node']->printPlus(); ?>

	 	</td>
	 	<td class='xb' name="treeNode[label][<?php echo $this->_tpl_vars['node']->name; ?>
]">
	  	  <?php echo $this->_tpl_vars['node']->printLabel(); ?>

	  	</td>
	  </tr>
	  <tr <?php if ($this->_tpl_vars['node']->contracted): ?>style="display:none"<?php endif; ?>>
	  	<td class="d"></td>
		  <td class="xd" name="treeNode[content][<?php echo $this->_tpl_vars['node']->name; ?>
]">
  		  <?php echo $this->_tpl_vars['node']->printContent(); ?>

  		</td>
	  </tr>
  </table>
	</div>
	<div class="boxBottomBlue">	
		<div class="a"></div>
		<div class="b"></div>
		<div class="c"></div>	
	</div>
</div>
<?php elseif (( $this->_tpl_vars['node']->nodeType === 'child' )): ?>
<div class="treeChild">
<table id="treeNode[<?php echo $this->_tpl_vars['node']->name; ?>
]" cellspacing="0" cellpadding="0" border="0" style="width:<?php echo $this->_tpl_vars['node']->width; ?>
;border-collapse:collapse;">
  <tr <?php if ($this->_tpl_vars['node']->value == ''): ?>style="display:none"<?php endif; ?>>
  	<td class="a" name="treeNode[plus][<?php echo $this->_tpl_vars['node']->name; ?>
]" width="1px">
  		<?php echo $this->_tpl_vars['node']->printPlus(); ?>

  	</td>
  	<td class="b" name="treeNode[label][<?php echo $this->_tpl_vars['node']->name; ?>
]">
 	  <?php echo $this->_tpl_vars['node']->printLabel(); ?>

  	</td>
  </tr>
  <tr <?php if ($this->_tpl_vars['node']->contracted): ?>style="display:none"<?php endif; ?>>
  	<td class="xc"></td>
	  <td class="xd" name="treeNode[content][<?php echo $this->_tpl_vars['node']->name; ?>
]">
  	  <?php echo $this->_tpl_vars['node']->printContent(); ?>

  	</td>
  </tr>
</table>
</div>
<?php elseif (( $this->_tpl_vars['node']->nodeType === 'blank' )): ?>
<div class="treeBlank">
<table id="treeNode[<?php echo $this->_tpl_vars['node']->name; ?>
]" cellspacing="0" cellpadding="0" border="0" style="width:<?php echo $this->_tpl_vars['node']->width; ?>
;border-collapse:collapse;">
  <tr <?php if ($this->_tpl_vars['node']->value == ''): ?>style="display:none"<?php endif; ?>>
  	<td class="a" name="treeNode[plus][<?php echo $this->_tpl_vars['node']->name; ?>
]" width="1px">
  		<?php echo $this->_tpl_vars['node']->printPlus(); ?>

  	</td>
  	<td class="b" name="treeNode[label][<?php echo $this->_tpl_vars['node']->name; ?>
]">
 	  <?php echo $this->_tpl_vars['node']->printLabel(); ?>

  	</td>
  </tr>
  <tr <?php if ($this->_tpl_vars['node']->contracted): ?>style="display:none"<?php endif; ?>>
  	<td class="xc"></td>
	  <td class="xd" name="treeNode[content][<?php echo $this->_tpl_vars['node']->name; ?>
]">
  	  <?php echo $this->_tpl_vars['node']->printContent(); ?>

  	</td>
  </tr>
 </table>
</div>
<?php endif; ?>