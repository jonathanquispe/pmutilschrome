<?php /* Smarty version 2.6.18, created on 2014-11-27 17:32:58
         compiled from layout.html */ ?>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <?php echo $this->_tpl_vars['meta']; ?>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon" />
    <?php echo $this->_tpl_vars['header']; ?>

  </head>
  <body>
    <table width="100%" height="100%" cellpadding="0" cellspacing="0" border="0" id="pm_main_table">
      <tr>
        <td id="pm_header" valign="top">
          <table width="100%" height="32" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
            <tr>
                <?php if ($this->_tpl_vars['user_logged'] != '' || $this->_tpl_vars['tracker'] != ''): ?>
                <td width="50%" rowspan="2" valign="center"><img src="<?php echo $this->_tpl_vars['logo_company']; ?>
" class="logo_company"/></td>
                <td width="50%" height="16" align="right" valign="top">
                  <div align="right" class="logout">
                    <small>
                    <?php if ($this->_tpl_vars['user_logged'] != ''): ?>
                      <?php echo $this->_tpl_vars['msgVer']; ?>
<label class="textBlue"><?php echo $this->_tpl_vars['userfullname']; ?>
 <a href="../users/myInfo"><?php echo $this->_tpl_vars['user']; ?>
</a> | </label>
                      <?php if ($this->_tpl_vars['switch_interface']): ?>
                      <label class="textBlue"><a href="../../uxs/home"><?php echo $this->_tpl_vars['switch_interface_label']; ?>
</a> | </label>
                      <?php endif; ?>
                      <a href="<?php echo $this->_tpl_vars['linklogout']; ?>
" class="tableOption"><?php echo $this->_tpl_vars['logout']; ?>
</a>&nbsp;&nbsp;<br/>
                      <label class="textBlack"><b><?php echo $this->_tpl_vars['rolename']; ?>
</b> <?php echo $this->_tpl_vars['workspace_label']; ?>
 <b><u><?php echo $this->_tpl_vars['workspace']; ?>
</u></b> &nbsp; &nbsp; <br/>
                      <?php echo $this->_tpl_vars['udate']; ?>
</label>&nbsp; &nbsp;
                    <?php else: ?>
                        <?php if ($this->_tpl_vars['tracker'] == 1): ?>
                            <a href="<?php echo $this->_tpl_vars['linklogout']; ?>
" class="tableOption"><?php echo $this->_tpl_vars['logout']; ?>
</a>&nbsp;&nbsp;
                        <?php endif; ?>
                    <?php endif; ?>
                    </small>
                  </div>
                </td>
                <?php else: ?>
                <td width="100%" style="padding-top: 10px"><img style="display: block; margin-left: auto; margin-right: auto;" src="<?php echo $this->_tpl_vars['logo_company']; ?>
"/></td>
                <?php endif; ?>
            </tr>
            <tr>
              <td width="50%" height="16" valign="bottom" class="title">
                <div align="right"></div>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td>
          <table width="100%" cellspacing="0" cellpadding="0" border="0" >
            <tr>
              <?php if ($this->_tpl_vars['user_logged'] != '' || $this->_tpl_vars['tracker'] != ''): ?>
              <td width="100%" class="mainMenuBG" style="height:25px">
                <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['tpl_menu']), 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
              </td>
              <?php else: ?>
              <td width="100%" style="height:25px">
              </td>
              <?php endif; ?>
            </tr>
            <?php if (( count ( $this->_tpl_vars['subMenus'] ) > 0 )): ?>
            <tr>
              <td >
              <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr><td width="100%" class="mainMenuBG"><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['tpl_submenu']), 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></td></tr>
              </table>
              </td>
            </tr>
            <?php endif; ?>
            <tr>
              <td width="100%" align="center">
                <?php 
                global $G_TEMPLATE;
                if ($G_TEMPLATE != '') G::LoadTemplate($G_TEMPLATE);
                 ?>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr height="100%">
        <td height="100%">
          <div class="Footer">
            <div class="content"><?php echo $this->_tpl_vars['footer']; ?>
</div>
          </div>
        </td>
      </tr>
    </table>
  </body>
</html>