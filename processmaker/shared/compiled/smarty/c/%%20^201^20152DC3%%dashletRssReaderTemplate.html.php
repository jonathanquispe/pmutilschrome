<?php /* Smarty version 2.6.18, created on 2014-11-27 11:25:12
         compiled from dashletRssReaderTemplate.html */ ?>
<!--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">-->
<!--<html xmlns="http://www.w3.org/1999/xhtml">-->
<head>
  <title></title>
  <meta http-equiv="refresh" content="30;url="$url> 
  <style type="text/css">
  <?php echo '
      body{
        margin: 0;
        padding: 0;

        background: #FFFFFF;
        font: 100% arial, verdana, helvetica, sans-serif;
        color: #000000;
      }
    #container{
      margin: 0 auto;
    }
    .clearf{
      clear: both;
      height: 0;
      line-height: 0;
      font-size: 0;
    }
    .icon{
      float:left;
      margin-left: 5%;
      width: 13%;
    }
    .description{
      float: right;
      margin-right: 1%;
      width: 90%;
      font-size: 64%;
      text-align : justify;
    }

    .icon img{
      width: 35px;
    }

    .description strong{
      color: #2C2C2C;
    }

    .description a{
      color: #1A4897;
      font-weight: bold;
    }

    .icon, .description{
      margin-top: 0.20em;
    }
    error{
    font-family:verdana;
    font-size:14px;
    color:red;
    /*background-color:maroon;*/
    }
    /*TD{font-family: arial,verdana,helvetica,sans-serif; font-size: 8pt;*/
    /*}*/
    '; ?>

  </style>
</head>
<body>
<table cellspacing="0" cellpadding="0" border="0" width="99%">

<?php if ($this->_tpl_vars['status'] == 'Error'): ?>
  <br/><br/><br/>
  <tr>
    <td style=" font-family:verdana; font-size:22px; color:Red;" align="center"><?php echo $this->_tpl_vars['render'][0]['link']; ?>
</td>
  </tr>
  <tr >
    <td style=" font-family:verdana; font-size:19px; color:Red;" align="center"><?php echo $this->_tpl_vars['render'][0]['description']; ?>
</td>
  </tr>
<?php else: ?>
  <?php unset($this->_sections['index']);
$this->_sections['index']['name'] = 'index';
$this->_sections['index']['loop'] = is_array($_loop=$this->_tpl_vars['render']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['index']['show'] = true;
$this->_sections['index']['max'] = $this->_sections['index']['loop'];
$this->_sections['index']['step'] = 1;
$this->_sections['index']['start'] = $this->_sections['index']['step'] > 0 ? 0 : $this->_sections['index']['loop']-1;
if ($this->_sections['index']['show']) {
    $this->_sections['index']['total'] = $this->_sections['index']['loop'];
    if ($this->_sections['index']['total'] == 0)
        $this->_sections['index']['show'] = false;
} else
    $this->_sections['index']['total'] = 0;
if ($this->_sections['index']['show']):

            for ($this->_sections['index']['index'] = $this->_sections['index']['start'], $this->_sections['index']['iteration'] = 1;
                 $this->_sections['index']['iteration'] <= $this->_sections['index']['total'];
                 $this->_sections['index']['index'] += $this->_sections['index']['step'], $this->_sections['index']['iteration']++):
$this->_sections['index']['rownum'] = $this->_sections['index']['iteration'];
$this->_sections['index']['index_prev'] = $this->_sections['index']['index'] - $this->_sections['index']['step'];
$this->_sections['index']['index_next'] = $this->_sections['index']['index'] + $this->_sections['index']['step'];
$this->_sections['index']['first']      = ($this->_sections['index']['iteration'] == 1);
$this->_sections['index']['last']       = ($this->_sections['index']['iteration'] == $this->_sections['index']['total']);
?>
  <tr >
    <td class="description a"><?php echo $this->_tpl_vars['render'][$this->_sections['index']['index']]['link']; ?>
</td>
  </tr>
  <tr >
    <td class="description"><?php echo $this->_tpl_vars['render'][$this->_sections['index']['index']]['description']; ?>
</td>
  </tr>
  <?php endfor; endif; ?>
<?php endif; ?>


</table>

</body> 

<!--</html>-->