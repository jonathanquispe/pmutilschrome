<?php /* Smarty version 2.6.18, created on 2014-11-27 10:06:44
         compiled from mem:defaultTemplateprocesses_Edit */ ?>
<form id="<?php echo $this->_tpl_vars['form_id']; ?>
" name="<?php echo $this->_tpl_vars['form_name']; ?>
" action="<?php echo $this->_tpl_vars['form_action']; ?>
" class="<?php echo $this->_tpl_vars['form_className']; ?>
" method="post" encType="multipart/form-data" style="margin:0px;" onsubmit='return validateForm("<?php echo $this->_tpl_vars['form_objectRequiredFields']; ?>
".parseJSON());'>
<div class="borderForm" style="padding-left: 0pt; padding-right: 0pt;">
  <div class="boxTop"><div class="a"></div><div class="b"></div><div class="c"></div></div>
    <div class="content" style="">
    <table width="99%">
    <tbody>
    <tr>
      <td valign="top">
        <input class="notValidateThisFields" name="__notValidateThisFields__" id="__notValidateThisFields__" value="" type="hidden">
        <input name="DynaformRequiredFields" id="DynaformRequiredFields" value="<?php echo $this->_tpl_vars['form_objectRequiredFields']; ?>
" type="hidden">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tbody>
        <tr>
          <td class="FormTitle" colspan="2" align=""><?php echo $this->_tpl_vars['form']['TITLE1']; ?>
</td>
        </tr>
        <tr style="display: none;">
          <td colspan="2"><?php echo $this->_tpl_vars['form']['PRO_UID']; ?>
</td>
        </tr>
        <tr style="display: none;">
          <td colspan="2"><?php echo $this->_tpl_vars['form']['THETYPE']; ?>
</td>
        </tr>
        <tr style="display: none;">
          <td colspan="2"><?php echo $this->_tpl_vars['form']['SYS_LANG']; ?>
 <?php echo $this->_tpl_vars['form']['PRO_VALIDATE_TITLE']; ?>
</td>
        </tr>
        <tr>
          <td class="FormLabel" width="<?php echo $this->_tpl_vars['form_labelWidth']; ?>
"><font color="red">*  </font><?php echo $this->_tpl_vars['PRO_TITLE']; ?>
</td>
          <td class="FormFieldContent" width="<?php echo $this->_tpl_vars['form_fieldContentWidth']; ?>
"><?php echo $this->_tpl_vars['form']['PRO_TITLE']; ?>
</td>
        </tr>
        <tr>
          <td class="FormLabel" width="<?php echo $this->_tpl_vars['form_labelWidth']; ?>
"><?php echo $this->_tpl_vars['PRO_DESCRIPTION']; ?>
</td>
          <td class="FormFieldContent" width="<?php echo $this->_tpl_vars['form_fieldContentWidth']; ?>
"><?php echo $this->_tpl_vars['form']['PRO_DESCRIPTION']; ?>
</td>
        </tr>
        <tr>
          <td class="FormLabel" width="<?php echo $this->_tpl_vars['form_labelWidth']; ?>
"><?php echo $this->_tpl_vars['PRO_CALENDAR']; ?>
</td>
          <td class="FormFieldContent" width="<?php echo $this->_tpl_vars['form_fieldContentWidth']; ?>
"><?php echo $this->_tpl_vars['form']['PRO_CALENDAR']; ?>
</td>
        </tr>
        <tr>
          <td class="FormLabel" width="<?php echo $this->_tpl_vars['form_labelWidth']; ?>
"><?php echo $this->_tpl_vars['PRO_CATEGORY']; ?>
</td>
          <td class="FormFieldContent" width="<?php echo $this->_tpl_vars['form_fieldContentWidth']; ?>
"><?php echo $this->_tpl_vars['form']['PRO_CATEGORY']; ?>
</td>
        </tr>
        <tr>
          <td class="FormLabel" width="<?php echo $this->_tpl_vars['form_labelWidth']; ?>
"><?php echo $this->_tpl_vars['PRO_SUMMARY_DYNAFORM']; ?>
</td>
          <td class="FormFieldContent" width="<?php echo $this->_tpl_vars['form_fieldContentWidth']; ?>
"><?php echo $this->_tpl_vars['form']['PRO_SUMMARY_DYNAFORM']; ?>
</td>
        </tr>
        <tr>
          <td class="FormLabel" width="<?php echo $this->_tpl_vars['form_labelWidth']; ?>
"><?php echo $this->_tpl_vars['PRO_DERIVATION_SCREEN_TPL']; ?>
</td>
          <td class="FormFieldContent" width="<?php echo $this->_tpl_vars['form_fieldContentWidth']; ?>
"><?php echo $this->_tpl_vars['form']['PRO_DERIVATION_SCREEN_TPL']; ?>
</td>
        </tr>
        <tr>
          <td class="FormLabel" width="<?php echo $this->_tpl_vars['form_labelWidth']; ?>
"><?php echo $this->_tpl_vars['PRO_DEBUG']; ?>
</td>
          <td class="FormFieldContent" width="<?php echo $this->_tpl_vars['form_fieldContentWidth']; ?>
"><?php echo $this->_tpl_vars['form']['PRO_DEBUG']; ?>
</td>
        </tr>
        <tr>
          <td class="FormLabel" width="<?php echo $this->_tpl_vars['form_labelWidth']; ?>
"><?php echo $this->_tpl_vars['PRO_SHOW_MESSAGE']; ?>
</td>
          <td class="FormFieldContent" width="<?php echo $this->_tpl_vars['form_fieldContentWidth']; ?>
"><?php echo $this->_tpl_vars['form']['PRO_SHOW_MESSAGE']; ?>
</td>
        </tr>
        <tr>
          <td class="FormLabel" width="<?php echo $this->_tpl_vars['form_labelWidth']; ?>
"><?php echo $this->_tpl_vars['PRO_SUBPROCESS']; ?>
</td>
          <td class="FormFieldContent" width="<?php echo $this->_tpl_vars['form_fieldContentWidth']; ?>
"><?php echo $this->_tpl_vars['form']['PRO_SUBPROCESS']; ?>
</td>
        </tr>
        <tr>
          <td class="FormLabel" width="<?php echo $this->_tpl_vars['form_labelWidth']; ?>
"><?php echo $this->_tpl_vars['PRO_TRI_DELETED']; ?>
</td>
          <td class="FormFieldContent" width="<?php echo $this->_tpl_vars['form_fieldContentWidth']; ?>
"><?php echo $this->_tpl_vars['form']['PRO_TRI_DELETED']; ?>
</td>
        </tr>
        <tr>
          <td class="FormLabel" width="<?php echo $this->_tpl_vars['form_labelWidth']; ?>
"><?php echo $this->_tpl_vars['PRO_TRI_CANCELED']; ?>
</td>
          <td class="FormFieldContent" width="<?php echo $this->_tpl_vars['form_fieldContentWidth']; ?>
"><?php echo $this->_tpl_vars['form']['PRO_TRI_CANCELED']; ?>
</td>
        </tr>
        <tr>
          <td class="FormLabel" width="<?php echo $this->_tpl_vars['form_labelWidth']; ?>
"><?php echo $this->_tpl_vars['PRO_TRI_PAUSED']; ?>
</td>
          <td class="FormFieldContent" width="<?php echo $this->_tpl_vars['form_fieldContentWidth']; ?>
"><?php echo $this->_tpl_vars['form']['PRO_TRI_PAUSED']; ?>
</td>
        </tr>
        <tr>
          <td class="FormLabel" width="<?php echo $this->_tpl_vars['form_labelWidth']; ?>
"><?php echo $this->_tpl_vars['PRO_TRI_REASSIGNED']; ?>
</td>
          <td class="FormFieldContent" width="<?php echo $this->_tpl_vars['form_fieldContentWidth']; ?>
"><?php echo $this->_tpl_vars['form']['PRO_TRI_REASSIGNED']; ?>
</td>
        </tr>
        <tr>
          <td class="FormLabel" width="<?php echo $this->_tpl_vars['form_labelWidth']; ?>
"><?php echo $this->_tpl_vars['PRO_TRI_UNPAUSED']; ?>
</td>
          <td class="FormFieldContent" width="<?php echo $this->_tpl_vars['form_fieldContentWidth']; ?>
"><?php echo $this->_tpl_vars['form']['PRO_TRI_UNPAUSED']; ?>
</td>
        </tr>
        <tr>
          <td class="FormLabel" width="<?php echo $this->_tpl_vars['form_labelWidth']; ?>
"><?php echo $this->_tpl_vars['PRO_TYPE_PROCESS']; ?>
</td>
          <td class="FormFieldContent" width="<?php echo $this->_tpl_vars['form_fieldContentWidth']; ?>
"><?php echo $this->_tpl_vars['form']['PRO_TYPE_PROCESS']; ?>
</td>
        </tr>
        <tr>
          <td class="FormButton" colspan="2" align="center"><br/> <?php echo $this->_tpl_vars['form']['SUBMIT']; ?>
 &nbsp; <?php echo $this->_tpl_vars['form']['BTN_CANCEL']; ?>
 </td>
        </tr>
        </tbody>
        </table>
      </td>
    </tr>
    </tbody>
    </table>
    <div class="FormRequiredTextMessage"><font color="red">*  </font><?php echo (G::LoadTranslation('ID_REQUIRED_FIELD')); ?></div>
  </div>
  <div class="boxBottom"><div class="a"></div><div class="b"></div><div class="c"></div>
</div>
<script type="text/javascript">
<?php echo $this->_tpl_vars['form']['JS']; ?>

</script>
</form>