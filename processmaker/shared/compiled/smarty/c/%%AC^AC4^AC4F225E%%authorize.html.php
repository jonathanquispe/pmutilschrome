<?php /* Smarty version 2.6.18, created on 2014-11-26 14:05:24
         compiled from C:%5CUsers%5CRichard%5CAppData%5CRoaming%5CProcessMaker-2_8_bdf253d-1%5Cprocessmaker%5Cworkflow%5Cengine%5Ctemplates/oauth2/authorize.html */ ?>
<!doctype html>
<html>
<head>
    <title>ProcessMaker Oauth2 Server</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Style sheets -->
    <link rel="stylesheet" type="text/css" href="/assets/css/pure-min.css">
    <link rel="stylesheet" href="/assets/css/base-min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/grids-responsive-min.css">
    <script type="text/javascript" src="/assets/js/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/assets/css/oauth2.css">
</head>
<body>

<form action="<?php echo $this->_tpl_vars['postUri']; ?>
" method="post" class="pure-form pure-form-stacked" enctype="application/x-www-form-urlencoded">
    <input name="transaction_id" type="hidden" value="<?php echo $this->_tpl_vars['transactionID']; ?>
">
    <fieldset>
        <!-- Upper box with Title, username, and password -->
        <div class="upper-box">

            <!-- Title -->
            <div style="padding-top: 15px">
                <img style="display: block; margin-left: auto; margin-right: auto;" src="/images/processmaker.logo.jpg">
                <div class="subtext">Authorization Server</div>
            </div>
            <div class="pure-control-group labels">
                <p>
                <p><?php echo $this->_tpl_vars['user']['name']; ?>
,</p>
                The application <b><?php echo $this->_tpl_vars['client']['name']; ?>
</b> is requesting access to your account.

                <h4><?php echo $this->_tpl_vars['client']['client_name']; ?>
</h4>
                <small><i><?php echo $this->_tpl_vars['client']['desc']; ?>
</i></small>
                </p>

                <p>Do you approve?</p>
            </div>
        </div>
        <!-- Bottom box with Sign in -->
        <div class="bottom-box">
            <div class="pure-controls accept-cancel-buttons">
                <div>
                    <input class="pure-button pure-button-primary" type="submit" value="Accept" id="allow">
                    <!-- Cheat to make a bit of spacing -->
                    <span>&nbsp&nbsp</span>
                    <input class="pure-button" type="submit" value="Deny" name="cancel" id="deny">
                </div>
            </div>
        </div>
    </fieldset>

</form>