'use strict'
var mongoskin = require('mongoskin');
module.exports = function() {
	var that = this;
	this.db="challenge",
	this.port="27017",
	this.host="localhost",
	this.dbmongo = mongoskin.db(
		"mongodb://"+this.host+":"+this.port+"/"+this.db, 
		{native_parser:true}
	);
};
