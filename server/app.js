var express  = require('express');
var app = express(); 
var skin =  require("./mongo/connection.js")
var mongodb = new skin();
var morgan   = require('morgan');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var Promise = require('promise');

var http = require('http').Server(app);
var io = require('socket.io')(http);

app.use(express.static(__dirname + '/public'));         
app.use(morgan('dev')); 
app.use(bodyParser.urlencoded({'extended':'true'})); 
app.use(bodyParser.json()); 
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); 
app.use(methodOverride('X-HTTP-Method-Override')); 

require('./routes/routes.js')(app,mongodb,io);
require('./routes/socket.js')(io,mongodb);

http.listen(4000, function (){
    console.log("App listening on port 4000");    
});

